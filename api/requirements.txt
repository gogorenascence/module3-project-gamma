fastapi[all]==0.92.0
uvicorn[standard]==0.22.0
pytest==7.3.1
psycopg[binary, pool]==3.1.9
requests==2.30.0
jwtdown-fastapi>=0.5.0
pydantic==1.10.7
