from fastapi import Depends, APIRouter, HTTPException
from queries.locations import APIQuery
from pydantic import BaseModel
from authenticator import authenticator


router = APIRouter()


class Field(BaseModel):
    x: str


class FieldDetail(BaseModel):
    x: str


@router.get("api/")
def get_all(
    repo: APIQuery = Depends(),
    account_data: dict = Depends(authenticator.get_account_data),
):
    if account_data:
        return repo.get_all_categories()
    raise HTTPException(status_code=404, detail="Not logged in")


@router.get("api/")
def get_one(
    category: str,
    repo: APIQuery = Depends(),
    account_data: dict = Depends(authenticator.get_account_data),
):
    if account_data:
        return repo.get_one_field(category)
    raise HTTPException(status_code=404, detail="Not logged in")


@router.get("api/")
def get_details(
    id: int,
    repo: APIQuery = Depends(),
    account_data: dict = Depends(authenticator.get_account_data),
):
    if account_data:
        try:
            return repo.get_details(id)
        except KeyError:
            raise HTTPException(status_code=404, detail="Doesn't exist")
    raise HTTPException(status_code=404, detail="Not logged in")
