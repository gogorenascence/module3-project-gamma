from fastapi.testclient import TestClient
from main import app
from queries.reviews import ReviewOut, ReviewQueries
from authenticator import authenticator


client = TestClient(app)


def mock_get_current_account_data():
    assert {
        "id": 0,
        "review": "string",
        "rating": "integer",
        "activity_id": "integer",
    }


class MockReviewRepo:
    def get_all_reviews(self):
        return [
            ReviewOut(id=0, review="one", rating=1, activity_id=0),
            ReviewOut(id=1, review="two", rating=5, activity_id=1),
        ]


def test_get_all_reviews():
    app.dependency_overrides[
        authenticator.get_account_data
    ] = mock_get_current_account_data
    app.dependency_overrides[ReviewQueries] = MockReviewRepo

    response = client.get("/api/reviews/")
    data = response.json()

    assert response.status_code == 200
    assert len(data) == 2
    assert data[0]["review"] == "one"
    assert data[1]["review"] == "two"

    app.dependency_overrides = {}
