Module3-Project-Gamma: Development Journal

May 17, 2023

Jason Anderson performed a merge action on the branch 'jason'. Further actions by Jason hinted at issues with code synchronization with the main branch. Meanwhile, Ricky Trang worked on editing the accounts.py router and resolving issues with the docker-compose file.

May 18, 2023

Jason Anderson continued to enhance the signup/login feature and addressed issues with the GHI container. He also merged changes from the main branch into the 'jason' branch.

May 22, 2023

Jason Anderson worked on resolving issues related to the signup process, while Joseph merged changes from the main branch into his own.

May 24, 2023

I merged the 'main' branch into my own branch, 'Dillon'. Jason saved progress before my merge and co-authored a commit with Ricky Trang. Ricky made improvements to account queries.

May 25, 2023

Ricky Trang worked on creating pins, and added detail to location along with making some CSS changes.

May 26, 2023

I introduced a 'Weather Man' feature in the application.

May 31, 2023

Ricky Trang addressed some minor changes resulting from a previous merge. Jason Anderson worked on authentication, observing some unexpected behavior with username and email fields on the login page.

June 1, 2023

Jason Anderson worked on logout functionality and a form associated with it, and merged changes from the 'jason' branch into 'main'.

June 2, 2023

Ricky Trang fixed styling for the login, signup, and logout functions and fixed a typo error in queries. Cyrus and I also pushed updates. Jason Anderson merged the 'Ricky' branch into 'main'.

June 3, 2023

Jason Anderson worked on changing accounts.py queries and creating a migration table's form for activity creation. I pushed a JSON update and a subsequent push named "June 3 Jason push to main".

June 4, 2023

Jason Anderson addressed getting activities to create in swagger, while Cyrus continued working on his branch.

June 5, 2023

I updated favorites, while Joseph completed the chatbot feature. Cyrus and Joseph merged changes from their branches into 'main'.

June 6, 2023

Ricky Trang worked on making activities and reviews endpoints functional. Joseph merged the 'main' branch into his own. I also merged 'main' into 'Dillon' branch.

June 7, 2023

I made contributions, identified as "Conditional Elements" and "Jun 7". Ricky Trang made improvements in pin mapping and added more to the journal. Jason Anderson merged the 'Dillon' branch into 'main'.

June 8, 2023

Ricky Trang created a test for "get one review", and added a new field to the table. Joseph worked on update functions and successfully merged the 'joseph' branch. Jason Anderson fixed a merge error, co-authored a commit with Cyrus, and deleted several files. Cyrus also merged changes from the 'Cyrus' branch into 'main'.

June 9, 2023

I made a contribution today in the form of writing a descriptive
README.md file that goes into depth regarding the specifics of our project.


This concludes the development journal of Dillon Dewundara for the stated period. The team has demonstrated consistent effort in enhancing the application, addressing bugs, and performing necessary testing.
