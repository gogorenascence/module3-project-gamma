import useToken from "@galvanize-inc/jwtdown-for-react";

const LogoutForm = () => {
    const{ logout } = useToken();

    const handleSubmit = (e) => {
    e.preventDefault();
    logout();
};

return (
    <div className="card text-bg-light mb-1">
    <h5 className="card-header">Logout</h5>
        <div className="card-body">
            <form onSubmit={(e) => handleSubmit(e)}>
                <div>
                    <button className="btn btn-danger" type="submit" value="logout">LogOut</button>
                </div>
            </form>
        </div>
    </div>
);
};

export default LogoutForm;
