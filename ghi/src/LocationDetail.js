import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";

function LocationDetail() {
  const { id } = useParams();
  const [activity, setActivity] = useState("");
  const [rating, setRating] = useState(0);
  const [reviews, setReviews] = useState([]);

  useEffect(() => {
  const fetchActivity = async () => {
    const response = await fetch(`${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}/api/activities/${id}`);
    if (response.ok) {
      const data = await response.json();
      setActivity(data);
    } else {
      console.error("Failed to fetch activities");
    }
  };

  const fetchRatings = async () => {
    const response = await fetch(
      `${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}/api/reviews/?activity_id=${id}`
    );
    if (response.ok) {
      const data = await response.json();
      let activity_ratings = 0;
        const activity_rating = data.filter((rating) => rating.activity_id === parseInt(id));
        for (let rating of activity_rating) {
          activity_ratings += rating.rating;
        }
      setRating(activity_ratings / activity_rating.length);
    } else {
      console.error("Failed to fetch ratings");
    }
  };

  const fetchReviews = async () => {
    const response = await fetch(
      `${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}/api/reviews/`
    );
    if (response.ok) {
      const data = await response.json();
      const activity_reviews = data.filter((review) => review.activity_id === parseInt(id));
      setReviews(activity_reviews);
    } else {
      console.error("Failed to fetch reviews");
    }
  };

    fetchActivity();
    fetchRatings();
    fetchReviews();
  }, [id]);


  return (
    <div className="container mt-5">
      <div className="row justify-content-center">
        <div className="col-md-8">
          <div className="card">
            <h4 className="card-header bg-dark text-white text-center">
              {activity.activity_name}
            </h4>
            <img src={activity.picture_url} className="card-img-top" alt={activity.activity_name} />
            <hr />
            <div className="card-body">
              <h5 className="card-title">Location Details</h5>
              <div className="card-text">
                <div key={activity.id} >
                  <p>
                    <strong>Name:</strong> {activity.activity_name}
                  </p>
                  <p>
                    <strong>Activity:</strong> {activity.activity_type}
                  </p>
                  <p>
                    <strong>Average Rating:</strong> {rating}
                  </p>
                  <p>
                    <strong>Hours of Operation:</strong> {activity.hours}
                  </p>
                </div>
                <div>
                  <h5>List of Reviews</h5>
                  {reviews.map((review) => (
                    <div key={review.id}>
                      <p>Review: {review.review}</p>
                      <p>Rating: {review.rating}</p>
                      <hr />
                    </div>
                  ))}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default LocationDetail;
