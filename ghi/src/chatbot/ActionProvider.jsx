import React from 'react';

const ActionProvider = ({ createChatBotMessage, setState, children }) => {
  const handleActivity = () => {
    const botMessage = createChatBotMessage('You can create activity under the "Create Activity" in the nav bar. You new activity will then be diplayed on the map!');

    setState((prev) => ({
      ...prev,
      messages: [...prev.messages, botMessage],
    }));
    
  };
  const handleView = () => {
    const botMessage = createChatBotMessage('You can view created activities from the map. Select a location to view more details.');

    setState((prev) => ({
      ...prev,
      messages: [...prev.messages, botMessage],
    }));
  };
    
  const handleReview = () => {
    const botMessage = createChatBotMessage('Once you have created an activity, you can add a review for that location under in the "Create Review" in the nav bar.');

    setState((prev) => ({
      ...prev,
      messages: [...prev.messages, botMessage],
    }));
  };

  const handleUnknown = () => {
    const botMessage = createChatBotMessage("I'm sorry, I don't understand. Could you please rephrase your question?");

    setState((prev) => ({
      ...prev,
      messages: [...prev.messages, botMessage],
    }));
  

  
    
  };


  return (
    <div>
      {React.Children.map(children, (child) => {
        return React.cloneElement(child, {
          actions: {
            handleActivity,
            handleView,
            handleReview,
            handleUnknown,
          },
        });
      })}
    </div>
  );
};

export default ActionProvider;